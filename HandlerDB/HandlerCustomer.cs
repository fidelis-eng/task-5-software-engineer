﻿using CustomerApi.Interfaces;
using CustomerApi.Model;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace CustomerApi.HandlerDB
{

    public class HandlerCustomer : ICustomerService
    {
        private readonly CustomerDBContext _dbContext;
        public HandlerCustomer(CustomerDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IEnumerable<Customer> GetAll()
        {
            List<Customer> response = new List<Customer>();
            var dataList = _dbContext.customers.ToList();
            dataList.ForEach(row => response.Add(new Customer()
            {
                ID = row.ID,
                FirstName = row.FirstName,
                LastName = row.LastName,
                Email = row.Email,
                HomeAddress = row.HomeAddress
            }));
            return response;
        }

        public Customer Get(string ID)
        {
            var row = _dbContext.customers.Where(d => d.ID.Equals(ID)).FirstOrDefault();
            if (row != null)
            {
                return new Customer()
                {
                    ID = row.ID,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    Email = row.Email,
                    HomeAddress = row.HomeAddress
                };
            };
            return null;
        }

        public Customer Create(Customer customer)
        {
            Guid uuidCustomer = Guid.NewGuid();
            string uuidCustomerString = uuidCustomer.ToString();

            Customer tempTable = new Customer();
            tempTable.ID = uuidCustomerString;
            tempTable.FirstName = customer.FirstName;
            tempTable.LastName = customer.LastName;
            tempTable.Email = customer.Email;
            tempTable.HomeAddress = customer.HomeAddress;

            _dbContext.customers.Add(tempTable);
            _dbContext.SaveChanges();

            return tempTable;
        }

        public bool Update(string id, Customer customer)
        {
            Customer tempTable = new Customer();
            tempTable = _dbContext.customers.Where(d => d.ID.Equals(id)).FirstOrDefault();
            if (tempTable != null)
            { 
                tempTable.FirstName= customer.FirstName;
                tempTable.LastName = customer.LastName;
                tempTable.Email =customer.Email;
                tempTable.HomeAddress = customer.HomeAddress;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Delete(string id)
        {
            var DataCustomer = _dbContext.customers.Where(d => d.ID.Equals(id)).FirstOrDefault();
            if (DataCustomer != null)
            {
                _dbContext.customers.Remove(DataCustomer); 
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
