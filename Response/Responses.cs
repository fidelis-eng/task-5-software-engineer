﻿namespace CustomerApi.Response
{
    public class Responses
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public int statusCode { get; set; }
        public object? ResponseData { get; set; }
    }
    public enum ResponseType
    {
        Success,
        NotFound,
        Failure
    }
}
