﻿namespace CustomerApi.Response
{
    public class ResponseHandler
    {
        public static Responses GetErrorResponse(Exception ex, int statusCode)
        {
            Responses response = new Responses();
            response.Code = "1";
            response.Message = ex.Message;
            response.ResponseData = "null";
            response.statusCode = statusCode;
            return response;
        }
        public static Responses GetSuccessResponse(ResponseType type, object? contract, int status)
        {
            Responses response;

            response = new Responses { ResponseData = contract, statusCode = status };

            switch (type)
            {
                case ResponseType.Success:
                    response.Code = "0";
                    response.Message = "Success";
                    break;
                case ResponseType.NotFound:
                    response.Code = "2";
                    response.Message = "No record available";
                    break;
            }
            return response;
        }
    }
}
