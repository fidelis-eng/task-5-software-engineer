﻿using CustomerApi.Model;

namespace CustomerApi.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        Customer Get(string id);
        Customer Create(Customer customer);
        bool Update(string id, Customer customer);
        bool Delete(string id);
    }

    public interface ITodoClient 
    {
        Task<IEnumerable<Todo>?> Get();
        object Get(int id);
    }

}
