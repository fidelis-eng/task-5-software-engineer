﻿
using CustomerApi.Interfaces;
using CustomerApi.Model;

namespace CustomerApi.Clients
{
    public class ClientTypicode : ITodoClient
    {
        private readonly HttpClient _httpClient;

        public ClientTypicode(HttpClient httpClient)
        {
            _httpClient = httpClient;

            _httpClient.BaseAddress = new Uri("https://jsonplaceholder.typicode.com");
        }
        public async Task<IEnumerable<Todo>?> Get() =>
            await _httpClient.GetFromJsonAsync<IEnumerable<Todo>>("todos");
        public object Get(int id) =>
            _httpClient.GetFromJsonAsync<Todo>("todos/" + id).Result;

    }
}
