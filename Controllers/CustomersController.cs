﻿using CustomerApi.HandlerDB;
using CustomerApi.Model;
using CustomerApi.Response;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{ 
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly HandlerCustomer _handlerCustomer;
        private readonly ILogger<CustomersController> _logger;
        public CustomersController(CustomerDBContext _dbContext, ILogger<CustomersController> logger)
        {
            _handlerCustomer = new HandlerCustomer(_dbContext);
            _logger = logger;   
        }

        // GET
        [HttpGet]
        public IActionResult Get()
        {
            ResponseType type = ResponseType.Success;
            try
            {
                IEnumerable<Customer>? data = _handlerCustomer.GetAll() as IEnumerable<Customer>;
                int statusCode = 0;
                if (!data.Any())
                {
                    statusCode = StatusCodes.Status202Accepted;
                    _logger.LogInformation("(GET) Show all customers data | data are empty");
                    type = ResponseType.NotFound;
                }
                else
                {
                    statusCode = StatusCodes.Status200OK;
                    _logger.LogInformation("(GET) Show all customers data | Successfully retrieved records. length =  " + data.Count());
                }
                return Ok(ResponseHandler.GetSuccessResponse(type, data,statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError("(GET) Show all customers data | Error message = "+ ex);
                int statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }
        }

        // GET(Id)
        [HttpGet("{ID}")]
        public IActionResult Get(string ID)
        {
            ResponseType type = ResponseType.Success;
            int statusCode = 0;
            try
            {
                Customer? data = _handlerCustomer.Get(ID) as Customer;
                if (data == null)
                {
                    statusCode = StatusCodes.Status202Accepted;
                    _logger.LogInformation($"(GET(id)) Get customer by ID | Not found customer with ID = {ID}");
                    type = ResponseType.NotFound;
                }
                else
                {
                    statusCode = StatusCodes.Status200OK;
                    _logger.LogInformation($"(GET(id)) Get customer by ID | Found customer with ID = {ID}");
                }
                return Ok(ResponseHandler.GetSuccessResponse(type, data,statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError($"(GET(id)) Get customer by ID | error message = {ex}");
                statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }
        }

        // POST
        [HttpPost]
        public IActionResult Post([FromBody] Customer data)
        {
            ResponseType type = ResponseType.Success;
            int statusCode = 0;
            try
            {
                Customer model = _handlerCustomer.Create(data);
                _logger.LogInformation($"(POST) Add customer | Successfully add customer with ID = {model.ID}");
                statusCode = StatusCodes.Status201Created;
                return Ok(ResponseHandler.GetSuccessResponse(type, model, statusCode));
            }
            catch(Exception ex)
            {
                _logger.LogError($"(POST) Add customer | Error message {ex}");
                statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }

        }

        // PUT 
        [HttpPut("{ID}")]
        public IActionResult Put(string ID, [FromBody] Customer data)
        {
            ResponseType type = ResponseType.Success;
            string successResponse = "";
            int statusCode = 0;
            try
            {
                var responseBool = _handlerCustomer.Update(ID, data);
                if (responseBool == false)
                {
                    statusCode = StatusCodes.Status202Accepted;
                    _logger.LogInformation($"(PUT) Update customer data | ID customer not found, {ID}");
                    successResponse = "Not found the record";
                    type = ResponseType.NotFound;
                }
                else
                {
                    statusCode = StatusCodes.Status200OK;
                    _logger.LogInformation($"(PUT) Update customer data | Successfully updated record with ID = {ID}");
                    successResponse = "Sucessfully updated record";
                }
                return Ok(ResponseHandler.GetSuccessResponse(type, successResponse,statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError($"(PUT) Update customer data | Error message {ID}");
                statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }
        }

        // DELETE 
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            string successResponse = "";
            int statusCode = 0;
            try
            {
                ResponseType type = ResponseType.Success;
                var responseBool = _handlerCustomer.Delete(id);
                if (responseBool == false)
                {
                    statusCode = StatusCodes.Status202Accepted;
                    _logger.LogInformation($"(DELETE) Delete customer data | ID customer not found, {id}");
                    successResponse = "Not found the record";
                    type = ResponseType.NotFound;
                }
                else
                {
                    statusCode = StatusCodes.Status200OK;
                    _logger.LogInformation($"(DELETE) Delete customer data | Successfully deleted record with ID = {id}");
                    successResponse = "Sucessfully deleted record";
                }
                return Ok(ResponseHandler.GetSuccessResponse(type, successResponse, statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError($"(DELETE) Delete customer data | Error message = {ex}");
                statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }
        }
    }
}