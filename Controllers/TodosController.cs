﻿using CustomerApi.Clients;
using CustomerApi.Model;
using CustomerApi.Response;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/todos")]
    [ApiController]
    public class TodosController : ControllerBase
    {
        private readonly ILogger<TodosController> _logger;
        private readonly ClientTypicode _httpClient;

        public TodosController(ClientTypicode httpClient, ILogger<TodosController> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }    

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            ResponseType type = ResponseType.Success;
            try
            {
                IEnumerable<Todo>? data = await _httpClient.Get();
                int statusCode = 0;
                if (!data.Any())
                {
                    statusCode = StatusCodes.Status202Accepted;
                    _logger.LogWarning("[GET] Get all todos | not found the records");
                    type = ResponseType.NotFound;
                }
                else
                {
                    statusCode = StatusCodes.Status200OK;
                    _logger.LogInformation("[GET] Get all todos | found the records. length = " + data.Count());
                }
                return Ok(ResponseHandler.GetSuccessResponse(type, data, statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError("[GET] Get all todos | Error message = " + ex);
                int statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex,statusCode));
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ResponseType type = ResponseType.Success;
            int statusCode = 0;
            try
            {
                Todo? data = _httpClient.Get(id) as Todo;
                _logger.LogInformation($"[GET(id)] Get todo by id | found record with id, {id}");
                statusCode = StatusCodes.Status200OK;
                return Ok(ResponseHandler.GetSuccessResponse(type, data, statusCode));
            }
            catch (Exception ex)
            {
                _logger.LogError($"[GET(id)] Get todo by id | not found the record with id. {id}\n Error message = {ex}");
                statusCode = StatusCodes.Status400BadRequest;
                return BadRequest(ResponseHandler.GetErrorResponse(ex, statusCode));
            }
        }
    }
}