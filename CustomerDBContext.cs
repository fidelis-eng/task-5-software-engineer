﻿using CustomerApi.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CustomerApi
{
    public class CustomerDBContext : DbContext
    {
        public CustomerDBContext(DbContextOptions<CustomerDBContext> options) : base(options)
        {
        }

        public DbSet<Customer> customers { get; set; }
    }
}
